var CONSTANTS = {
	// for admin operations
	ADMIN_SIGNUP_URL: '/admin/signup',
	ADMIN_LOGIN_URL: '/admin/login',
	ADD_COUNTRY_URL: '/admin/add-country',
	EDIT_COUNTRY_URL: '/admin/edit-country',
	GET_COUNTRIES_URL: '/admin/get-countries',
	DELETE_COUNTRY_URL: '/admin/delete-country',
	GET_USERS_URL: '/admin/get-users',
	ADD_PLACE_URL: '/admin/add-place',
	EDIT_PLACE_URL: '/admin/edit-place',
	GET_PLACES_URL: '/admin/get-places',
	ADD_PLACE_TO_EDITORS_PICK_URL: '/admin/add-place-to-editor-pick',
	DELETE_PLACE_URL: '/admin/delete-place',
	GET_EDITORS_PICK_URL: '/admin/get-editors-pick',
	GET_DASHBOARD_SUMMARY_URL: '/admin/get-dashboard-summary',
	GET_DASHBOARD_USERS_SUMMARY_URL: '/admin/get-dashboard-users-summary',
	GET_DASHBOARD_PLACES_SUMMARY_URL: '/admin/get-dashboard-places-summary',
	GET_DASHBOARD_EDITORS_PICK_SUMMARY_URL: '/admin/get-dashboard-editors-pick-summary',


	CLIENT_LOGIN_URL: '/client/login',
	CLIENT_SIGNUP_URL: '/client/signup',
	ADD_CLIENT_PLACE_URL: '/client/add-place',
	GET_CLIENT_PLACES_URL: '/client/get-places',
	GET_CLIENT_DASHBOARD_SUMMARY_URL: '/client/get-dashboard-summary',
	GET_CLIENT_DASHBOARD_PLACES_URL: '/client/get-client-dashboard-places-summary',

}
