getDashboardSummary();
getAllPlacesSummary();

function getDashboardSummary() {
	$.ajax({
        url: CONSTANTS.GET_CLIENT_DASHBOARD_SUMMARY_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	$(".number-of-places-res").html(response.data.numberOfPlace);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}

// function to get all places
function getAllPlacesSummary() {
	$.ajax({
		url: CONSTANTS.GET_CLIENT_DASHBOARD_PLACES_URL,
		type: 'GET',
		beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Bearer ")
		},
		success: function(response) {
			if (response.success) {
				var template = "";
				if (response.data.length > 0) {
					var counter = 1;
					template += `
					<table id="data-table" class="table">
						<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Country</th>
						  <th scope="col">State/City</th>
						  <th scope="col">Location</th>
						</tr>
						</thead>
						<tbody>
					`;

					response.data.forEach(function(item){
						var itemObj = JSON.stringify(item);
						var editorsPickStatement = "";
						if (item.place_editors_pick == 1) {
							editorsPickStatement = "remove from editors pick"
						} else {
							editorsPickStatement = "add to editors pick"
						}

						template += `
							<tr>
								<th scope="row">${counter}</th>
								<td>${truncateString(item.place_name, 15, 12)}</td>
								<td>${truncateString(item.place_country, 15, 12)}</td>
								<td>${truncateString(item.place_state, 15, 12)}</td>
								<td>${truncateString(item.place_location, 15, 12)}</td>
							</tr>
						`;
						counter ++;
					});

					template += `
							</tbody>
						</table>

						<a  href="/client/places" class="btn btn-sm btn-info">view more</a>
					`;
				} else {

				}

				$('.admin-places-summary-div').html(template);
			} else {

			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
		   console.log(XMLHttpRequest, textStatus, errorThrown);
		}
	});
}