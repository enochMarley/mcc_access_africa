$('.client-login-form').on('submit', function(event) {
	event.preventDefault();

	var clientLoginEmail = $('.client-login-email').val().trim(),
		clientLoginPassword = $('.client-login-password').val().trim();

	if (clientLoginEmail == "") {
		showWarningMessage('Please enter email address', 'Account login');
	} else if (clientLoginPassword == ""){
		showWarningMessage('Please enter password', 'Account login');
	} else {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			clientLoginEmail: clientLoginEmail,
			clientLoginPassword: clientLoginPassword
		}

		$.ajax({
            url: CONSTANTS.CLIENT_LOGIN_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, "Account signup");
                	setTimeout(function() {
                		window.location.href = '/client/dashboard';
                	});
                } else {
                	showErrorMessage(response.message, "Account signup")
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
});


$('.client-signup-form').on('submit', function(event) {
	event.preventDefault();

	var clientSignupUsername = $('.client-signup-username').val().trim(),
		clientSignupEmail = $('.client-signup-email').val().trim(),
		clientSignupPassword = $('.client-signup-password').val().trim(),
		clientSignupPasswordConf = $('.client-signup-confirm-password').val().trim();

	if (clientSignupUsername == "") {
		showWarningMessage('Please enter username', 'Account signup');
	} 
	else if (clientSignupEmail == 0) {
		showWarningMessage('Please enter email', 'Account signup');
	} else if (clientSignupPassword == ""){
		showWarningMessage('Please enter password', 'Account signup');
	} else if (clientSignupPassword.length < 0){
		showWarningMessage('Password length must be at least 8 charactes long', 'Account signup');
	}  else if (clientSignupPassword != clientSignupPasswordConf){
		showWarningMessage('Provided passwords do not match', 'Account signup');
	} else {
		$('.client-signup-form button[type="submit"]').html('CREATING ACCOUNT');
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			clientSignupUsername: clientSignupUsername,
			clientSignupEmail: clientSignupEmail,
			clientSignupPassword: clientSignupPassword,
		}

		$.ajax({
            url: CONSTANTS.CLIENT_SIGNUP_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, "Account signup");
                	setTimeout(function() {
                		window.location.href = '/client/dashboard';
                	});
                } else {
                	showErrorMessage(response.message, "Account signup")
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
})