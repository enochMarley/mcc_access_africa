getAllPlaces();

// function to get all coutries list for form
function getAllCountriesForForm() {
	$.ajax({
        url: CONSTANTS.GET_COUNTRIES_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	var template = '';
            	var counter = 1;

            	if (response.data.length > 0) {
            		template += `
            		   <option disabled selected>Select Country</option>
            		`;

            		response.data.forEach(function(item) {
            			template += `
	            		   <option value="${item.country_name}">${item.country_name}</option>
	            		`;

            		});
            	} else {
            		template += `
            		   <option disabled selected>No images available</option>
            		`;
            	}

            	$('.add-place-country, .edit-place-country').html(template);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}

// event to add places for to database
$('.add-place-form').on('submit', function(event) {
	event.preventDefault();

	var placeName = $('.add-place-name').val().trim(),
		placeCountry = $('.add-place-country').val().trim(),
		placeState = $('.add-place-state').val().trim(),
		placeLocation = $('.add-place-location').val().trim(),
		placeDescription = $('.add-place-description').val().trim();

		placeImages = document.getElementById('places-images').files;



	if (placeName == "") {
		showWarningMessage('Please enter name of place', 'Admin action');
	} else if (placeCountry == "") {
		showWarningMessage('Please enter country of place', 'Admin action');
	} else if (placeState == "") {
		showWarningMessage('Please enter state of place', 'Admin action');
	} else if (placeLocation == "") {
		showWarningMessage('Please enter location of place', 'Admin action');
	} else if (placeDescription == "") {
		showWarningMessage('Please enter description of place', 'Admin action');
	} else if (placeImages.length > 5) {
		showWarningMessage('Please select max of 5 images', 'Admin action');
	} else if (placeImages.length < 1) {
		showWarningMessage('Please select an image', 'Admin action');
	} else {
		formData = new FormData();
		formData.append('placeName', placeName);
		formData.append('placeCountry', placeCountry);
		formData.append('placeState', placeState);
		formData.append('placeLocation', placeLocation);
		formData.append('placeDescription', placeDescription);
		formData.append('_token', $('meta[name=csrf-token]').attr('content'));
		for (image in placeImages) {
			formData.append('placeImages[]', placeImages[image]);
		}

		$.ajax({
	        url: CONSTANTS.ADD_PLACE_URL,
	        type: 'POST',
	        data: formData,
            processData: false,
            contentType: false,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader ("Authorization", "Bearer ")
	        },
	        success: function(response) {
				if (response.success) {
					showSuccessMessage(response.message, "Admin action");
					$('#admin-add-place-modal').modal("hide");
					getAllPlaces();
				} else {
					showErrorMessage(response.message, "Admin action");
				}

	        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
	           console.log(XMLHttpRequest, textStatus, errorThrown);
	        }
	    });
	}

})


// event to edit places for to database
$('.edit-place-form').on('submit', function(event) {
	event.preventDefault();

	var placeName = $('.edit-place-name').val().trim(),
		placeCountry = $('.edit-place-country').val().trim(),
		placeState = $('.edit-place-state').val().trim(),
		placeLocation = $('.edit-place-location').val().trim(),
		placeDescription = $('.edit-place-description').val().trim(),
		placeId = $('.edit-place-id').val();



	if (placeName == "") {
		showWarningMessage('Please enter name of place', 'Admin action');
	} else if (placeCountry == "") {
		showWarningMessage('Please enter country of place', 'Admin action');
	} else if (placeState == "") {
		showWarningMessage('Please enter state of place', 'Admin action');
	} else if (placeLocation == "") {
		showWarningMessage('Please enter location of place', 'Admin action');
	} else if (placeDescription == "") {
		showWarningMessage('Please enter description of place', 'Admin action');
	} else {
		formData = new FormData();
		formData.append('placeName', placeName);
		formData.append('placeCountry', placeCountry);
		formData.append('placeState', placeState);
		formData.append('placeLocation', placeLocation);
		formData.append('placeDescription', placeDescription);
		formData.append('placeId', placeId);
		formData.append('_token', $('meta[name=csrf-token]').attr('content'));

		$.ajax({
	        url: CONSTANTS.EDIT_PLACE_URL,
	        type: 'POST',
	        data: formData,
            processData: false,
            contentType: false,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader ("Authorization", "Bearer ")
	        },
	        success: function(response) {
				if (response.success) {
					showSuccessMessage(response.message, "Admin action");
					$('#admin-edit-place-modal').modal("hide");
					getAllPlaces();
				} else {
					showErrorMessage(response.message, "Admin action");
				}

	        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
	           console.log(XMLHttpRequest, textStatus, errorThrown);
	        }
	    });
	}

})

// function to get all places
function getAllPlaces() {
	$.ajax({
		url: CONSTANTS.GET_PLACES_URL,
		type: 'GET',
		beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Bearer ")
		},
		success: function(response) {
			if (response.success) {
				var template = "";
				if (response.data.length > 0) {
					var counter = 1;
					template += `
					<table id="data-table" class="table">
						<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Country</th>
						  <th scope="col">State/City</th>
						  <th scope="col">Location</th>
						  <th scope="col">Description</th>
						  <th scope="col">Rating</th>
						  <th scope="col">Upload date</th>
						  <th scope="col">Actions</th>
						</tr>
						</thead>
						<tbody>
					`;

					response.data.forEach(function(item){
						var itemObj = JSON.stringify(item);
						var editorsPickStatement = "";
						if (item.place_editors_pick == 1) {
							editorsPickStatement = "remove from editors pick"
						} else {
							editorsPickStatement = "add to editors pick"
						}

						template += `
							<tr>
								<th scope="row">${counter}</th>
								<td>${truncateString(item.place_name, 15, 12)}</td>
								<td>${truncateString(item.place_country, 15, 12)}</td>
								<td>${truncateString(item.place_state, 15, 12)}</td>
								<td>${truncateString(item.place_location, 15, 12)}</td>
								<td>${truncateString(item.place_description, 15, 12)}</td>
								<td>${parseFloat(item.place_rating).toFixed(2)}</td>
								<td>${truncateString(item.place_upload_date, 10, 7)}</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										action
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="/admin/place-details/${item.place_id}"><small>view</small></a>
											<a class="dropdown-item" href="#"  onclick='editPlace(${itemObj})'><small>edit</small></a>
											<!-- <a class="dropdown-item" href="#" onclick='deletePlace(${itemObj})'><small>delete</small></a> -->
											<a class="dropdown-item" href="#" onclick='togglePlaceEditorsPick(${itemObj})'><small>${editorsPickStatement}</small></a>
										</div>
									</div>
								</td>
							</tr>
						`;
						counter ++;
					});

					template += `
							</tbody>
						</table>
					`;
				} else {

				}

				$('.places-res').html(template);
			} else {

			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
		   console.log(XMLHttpRequest, textStatus, errorThrown);
		}
	});
}

function togglePlaceEditorsPick(itemObj) {
	var confirmAction = confirm('Are you sure you want to continue this action ?');

	if (confirmAction) {
		var data = {
			placeId: itemObj.place_id,
			'_token': $('meta[name=csrf-token]').attr('content')
		}

		$.ajax({
	        url: CONSTANTS.ADD_PLACE_TO_EDITORS_PICK_URL,
	        type: 'POST',
	        data: data,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader ("Authorization", "Bearer ")
	        },
	        success: function(response) {
				if (response.success) {
					showSuccessMessage(response.message, "Admin action");
					getAllPlaces();
				} else {
					showErrorMessage(response.message, "Admin action");
				}

	        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
	           console.log(XMLHttpRequest, textStatus, errorThrown);
	        }
	    });
	}

}

function editPlace(itemObj){
	$(".prev-place-country").html(itemObj.place_country);
	$('.edit-place-id').val(itemObj.place_id);
	$(".edit-place-name").val(itemObj.place_name);
	$(".edit-place-state").val(itemObj.place_state);
	$(".edit-place-location").val(itemObj.place_location);
	$(".edit-place-description").val(itemObj.place_description);
	$("#admin-edit-place-modal").modal("show");
}

// function deletePlace(itemObj) {
// 	var confirmDelete = confirm('Are you sure you want to delete this item?');
//
// 	if (confirmDelete) {
// 		var data = {
// 			placeId: itemObj.place_id,
// 			'_token': $('meta[name=csrf-token]').attr('content')
// 		}
//
// 		$.ajax({
// 	        url: CONSTANTS.DELETE_PLACE_URL,
// 	        type: 'POST',
// 	        data: data,
// 	        beforeSend: function (xhr) {
// 	            xhr.setRequestHeader ("Authorization", "Bearer ")
// 	        },
// 	        success: function(response) {
// 				if (response.success) {
// 					showSuccessMessage(response.message, "Admin action");
// 					getAllPlaces();
// 				} else {
// 					showErrorMessage(response.message, "Admin action");
// 				}
//
// 	        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
// 	           console.log(XMLHttpRequest, textStatus, errorThrown);
// 	        }
// 	    });
// 	}
// }
