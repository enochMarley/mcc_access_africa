function getAllUsers() {
	$.ajax({
        url: CONSTANTS.GET_USERS_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	var template = '';
            	var counter = 1;

            	if (response.data.length > 0) {
            		template += `
            		    <h5 class='text-center'>List of Users</h5>
            			<table class="table table-hover table-sm">
						  	<thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Name</th>
							      <th scope="col">Email Address</th>
							      <th scope="col">Type</th>
							    </tr>
						  	</thead>
						  	<tbody>
            		`;

            		response.data.forEach(function(item) {
            			var itemObj = JSON.stringify(item);
            			var adminType = (item.admin_type == 1) ? 'Client' : 'Super Admin';

            			template += `
            				<tr>
						      <th scope="row">${counter}</th>
						      <td>${item.admin_username}</td>
						      <td>${item.admin_email}</td>
						      <td>${adminType}</td>
						    </tr>
            			`;

            			counter ++;
            		});

            		template += `
            			</tbody>
            			</table>
            		`;

            	} else {
            		template += `
            			<div class='jumbotron'>
            				<h5 class='text-center'>No users available</h5>
            			</div>
            		`;

            	}

            	$('.users-res').html(template);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}