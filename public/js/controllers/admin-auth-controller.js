// event to submit admin signup form
$('.admin-signup-form').on('submit', function(event) {
	event.preventDefault();

	var adminUsername = $('.admin-signup-username').val().trim(),
		adminEmail = $('.admin-signup-email').val().trim(),
		adminPassword = $('.admin-signup-password').val().trim(),
		adminPasswordConf = $('.admin-signup-password-conf').val().trim();

	if (adminUsername == "") {
		showWarningMessage('Please enter admin username', 'Admin signup');
	} else if (adminEmail == "") {
		showWarningMessage('Please enter admin username', 'Admin signup');
	} else if (adminPassword == "") {
		showWarningMessage('Please enter admin password', 'Admin signup');
	} else if (adminPasswordConf == "") {
		showWarningMessage('Please enter admin username', 'Admin signup');
	} else if (adminPassword != adminPasswordConf) {
		showWarningMessage('Provided password do not match', 'Admin signup');
	} else {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			adminUsername: adminUsername,
			adminEmail: adminEmail,
			adminPassword: adminPassword
		}

		$.ajax({
            url: CONSTANTS.ADMIN_SIGNUP_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, 'Admin signup');
                	setTimeout(function() {
                		window.location.href = '/admin/login';
                	}, 2000);
                } else {
                	showErrorMessage(response.message, 'Admin signup');
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
});


// event to submit admin login form
$('.admin-login-form').on('submit', function(event) {
	event.preventDefault();

	var adminEmail = $('.admin-login-email').val().trim(),
		adminPassword = $('.admin-login-password').val().trim();

	if (adminEmail == "") {
		showWarningMessage('Please enter admin username', 'Admin signup');
	} else if (adminPassword == "") {
		showWarningMessage('Please enter admin password', 'Admin signup');
	} else {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			adminEmail: adminEmail,
			adminPassword: adminPassword
		}

		$.ajax({
            url: CONSTANTS.ADMIN_LOGIN_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
            	console.log(response)
                if (response.success) {
                	showSuccessMessage(response.message, 'Admin signup');
                	setTimeout(function() {
                		window.location.href = '/admin/dashboard';
                	}, 2000);
                } else {
                	showErrorMessage(response.message, 'Admin signup');
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
})