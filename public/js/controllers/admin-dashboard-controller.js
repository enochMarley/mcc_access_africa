getDashboardSummary();
getDashboardUsersSummary();
getDashboardPlacesSummary();
getDashboardEditorsPickSummary();

function getDashboardSummary() {
	$.ajax({
        url: CONSTANTS.GET_DASHBOARD_SUMMARY_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	$(".number-of-users-res").html(response.data.numberOfUsers);
            	$(".number-of-places-res").html(response.data.numberOfPlace);
            	$(".number-of-countries-res").html(response.data.numberOfCountries);
            	$(".number-of-editors-pick-res").html(response.data.numberOfEditorsPick);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


function getDashboardUsersSummary() {
	$.ajax({
        url: CONSTANTS.GET_DASHBOARD_USERS_SUMMARY_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	var template = '';
            	var counter = 1;

            	if (response.data.length > 0) {
            		template += `
            		    <h5 class='text-center'><small>List of Users</small></h5>
            			<table class="table table-hover table-sm">
						  	<thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Name</th>
							      <th scope="col">Email Address</th>
							      <th scope="col">Type</th>
							    </tr>
						  	</thead>
						  	<tbody>
            		`;

            		response.data.forEach(function(item) {
            			var itemObj = JSON.stringify(item);
            			var adminType = (item.admin_type == 1) ? 'Client' : 'Super Admin';

            			template += `
            				<tr>
						      <th scope="row">${counter}</th>
						      <td>${item.admin_username}</td>
						      <td>${item.admin_email}</td>
						      <td>${adminType}</td>
						    </tr>
            			`;

            			counter ++;
            		});

            		template += `
            			</tbody>
            			</table>

            			<a  href="/admin/users" class="btn btn-sm btn-info">view more</a>
            		`;

            	} else {
            		template += `
            			<div class='jumbotron'>
            				<h5 class='text-center'>No Users Available</h5>
            			</div>
            		`;

            	}

            	$('.dashboard-users-res').html(template);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}

function getDashboardPlacesSummary() {
	$.ajax({
		url: CONSTANTS.GET_DASHBOARD_PLACES_SUMMARY_URL,
		type: 'GET',
		beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Bearer ")
		},
		success: function(response) {
			if (response.success) {
				var template = "";
				if (response.data.length > 0) {
					var counter = 1;
					template += `
					<h5 class='text-center'><small>Places</small></h5>
					<table id="data-table" class="table table-sm">
						<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Country</th>
						  <th scope="col">State/City</th>
						  <th scope="col">Location</th>
						</tr>
						</thead>
						<tbody>
					`;

					response.data.forEach(function(item){
						var itemObj = JSON.stringify(item);
						template += `
							<tr>
								<th scope="row">${counter}</th>
								<td>${truncateString(item.place_name, 15, 12)}</td>
								<td>${truncateString(item.place_country, 15, 12)}</td>
								<td>${truncateString(item.place_state, 15, 12)}</td>
								<td>${truncateString(item.place_location, 15, 12)}</td>
							</tr>
						`;
						counter ++;
					});

					template += `
							</tbody>
						</table>

						<a  href="/admin/places" class="btn btn-sm btn-info">view more</a>
					`;
				} else {

				}

				$('.dashboard-places-res').html(template);
			} else {

			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
		   console.log(XMLHttpRequest, textStatus, errorThrown);
		}
	});
}

function getDashboardEditorsPickSummary() {
	$.ajax({
        url: CONSTANTS.GET_DASHBOARD_EDITORS_PICK_SUMMARY_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
				var template = "";
				if (response.data.length > 0) {
					var counter = 1;
					template += `
					<h5 class='text-center'><small>Editor's Pick</small></h5>
					<table id="data-table" class="table table-sm">
						<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Country</th>
						  <th scope="col">State/City</th>
						  <th scope="col">Location</th>
						</tr>
						</thead>
						<tbody>
					`;

					response.data.forEach(function(item){
						var itemObj = JSON.stringify(item[0]);

						template += `
							<tr>
								<th scope="row">${counter}</th>
								<td>${truncateString(item[0].place_name, 15, 12)}</td>
								<td>${truncateString(item[0].place_country, 15, 12)}</td>
								<td>${truncateString(item[0].place_state, 15, 12)}</td>
								<td>${truncateString(item[0].place_location, 15, 12)}</td>
							</tr>
						`;
						counter ++;
					});

					template += `
							</tbody>
						</table>

						<a  href="/admin/editors-pick" class="btn btn-sm btn-info">view more</a>
					`;
				} else {
                    template += `
                        <h4 class='text-center text-grey'>No Editors Pick Available</h4>
                    `
				}

				$('.dashboard-editors-pick-res').html(template);
			} else {

			}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}