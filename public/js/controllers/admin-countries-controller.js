// event to submit country addition / creation form
$('.add-country-form').on('submit', function(event) {
	event.preventDefault();

	var countryName = $('.add-country-name').val().trim();

	if (countryName == "") {
		showWarningMessage('Please enter country name', 'Admin action');
	} else {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			countryName: countryName
		}

		$.ajax({
            url: CONSTANTS.ADD_COUNTRY_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, 'Admin action');
                	getAllCountries();
                	setTimeout(function() {
                		$('#admin-add-country-modal').modal('hide');
                	}, 2000);
                } else {
                	showErrorMessage(response.message, 'Admin action');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
});


// event to submit country update / edition form
$('.edit-country-form').on('submit', function(event) {
	event.preventDefault();

	var countryName = $('.edit-country-name').val().trim();
	var countryId = $('.edit-country-id').val().trim();

	if (countryName == "") {
		showWarningMessage('Please enter country name', 'Admin action');
	} else {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			countryName: countryName,
			countryId: countryId
		}

		$.ajax({
            url: CONSTANTS.EDIT_COUNTRY_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, 'Admin action');
                	getAllCountries();
                	setTimeout(function() {
                		$('#admin-edit-country-modal').modal('hide');
                	}, 2000);
                } else {
                	showErrorMessage(response.message, 'Admin action');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
});

// function to get all coutries
function getAllCountries() {
	$.ajax({
        url: CONSTANTS.GET_COUNTRIES_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
            	var template = '';
            	var counter = 1;

            	if (response.data.length > 0) {
            		template += `
            		    <h5 class='text-center'>List of Countries</h5>
            			<table class="table table-hover table-sm">
						  	<thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Country Name</th>
							      <th scope="col">Actions</th>
							    </tr>
						  	</thead>
						  	<tbody>
            		`;

            		response.data.forEach(function(item) {
            			var itemObj = JSON.stringify(item);

            			template += `
            				<tr>
						      <th scope="row">${counter}</th>
						      <td>${item.country_name}</td>
						      <td>
						      	<button class='btn btn-sm btn-info' onclick='showEditCountryForm(${itemObj})'>edit</button>&nbsp;&nbsp;&nbsp;
						      	<button class='btn btn-sm btn-danger'  onclick='showDeleteCountryWarning(${itemObj})'>delete</button>
						      </td>
						    </tr>
            			`;

            			counter ++;
            		});

            		template += `
            			</tbody>
            			</table>
            		`;

            	} else {
            		template += `
            			<div class='jumbotron'>
            				<h5 class='text-center'>No countries available</h5>
            			</div>
            		`;

            	}

            	$('.countries-res').html(template);
            } else {

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


// modal to show edit country form
function showEditCountryForm(itemObj) {
	$('.edit-country-name').val(itemObj.country_name);
	$('.edit-country-id').val(itemObj.country_id);
	
	$('#admin-edit-country-modal').modal('show');
}


// function to show delete country warning
function showDeleteCountryWarning(itemObj) {
	var deleteCountryConfirmation = confirm('Are you sure you want to delete this country form the database?');

	if (deleteCountryConfirmation) {
		var data = {
			'_token': $('meta[name=csrf-token]').attr('content'),
			countryId: itemObj.country_id
		}

		$.ajax({
            url: CONSTANTS.DELETE_COUNTRY_URL,
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer ")
            },
            success: function(response) {
                if (response.success) {
                	showSuccessMessage(response.message, 'Admin action');
                	getAllCountries();
                } else {
                	showErrorMessage(response.message, 'Admin action');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
               console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
	}
}