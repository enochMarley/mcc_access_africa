getEditorsPick();

function getEditorsPick() {
	$.ajax({
        url: CONSTANTS.GET_EDITORS_PICK_URL,
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer ")
        },
        success: function(response) {
            if (response.success) {
				var template = "";
				if (response.data.length > 0) {
					var counter = 1;
					template += `
					<table class="table">
						<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Country</th>
						  <th scope="col">State/City</th>
						  <th scope="col">Location</th>
						  <th scope="col">Description</th>
						  <th scope="col">Upload date</th>
						  <th scope="col">Actions</th>
						</tr>
						</thead>
						<tbody>
					`;

					response.data.forEach(function(item){
						var itemObj = JSON.stringify(item[0]);

						template += `
							<tr>
								<th scope="row">${counter}</th>
								<td>${truncateString(item[0].place_name, 15, 12)}</td>
								<td>${truncateString(item[0].place_country, 15, 12)}</td>
								<td>${truncateString(item[0].place_state, 15, 12)}</td>
								<td>${truncateString(item[0].place_location, 15, 12)}</td>
								<td>${truncateString(item[0].place_description, 15, 12)}</td>
								<td>${truncateString(item[0].place_upload_date, 10, 7)}</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										action
										</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#" onclick='togglePlaceEditorsPick(${itemObj})'><small>remove from editors pick</small></a>
										</div>
									</div>
								</td>
							</tr>
						`;
						counter ++;
					});

					template += `
							</tbody>
						</table>
					`;
				} else {
                    template += `
                        <h4 class='text-center text-grey'>No Editors Pick Available</h4>
                    `
				}

				$('.editors-pick-res').html(template);
			} else {

			}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
           console.log(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


function togglePlaceEditorsPick(itemObj) {
	var confirmAction = confirm('Are you sure you want to continue this action ?');

	if (confirmAction) {
		var data = {
			placeId: itemObj.place_id,
			'_token': $('meta[name=csrf-token]').attr('content')
		}

		$.ajax({
	        url: CONSTANTS.ADD_PLACE_TO_EDITORS_PICK_URL,
	        type: 'POST',
	        data: data,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader ("Authorization", "Bearer ")
	        },
	        success: function(response) {
				if (response.success) {
					showSuccessMessage(response.message, "Admin action");
					getEditorsPick();
				} else {
					showErrorMessage(response.message, "Admin action");
				}

	        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
	           console.log(XMLHttpRequest, textStatus, errorThrown);
	        }
	    });
	}

}
