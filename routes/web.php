<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
	return 'hello there';
});

Route::get('new/sigup', function(){
	return 'new user in the file';
});

// Routes for the client APIController
Route::prefix('api')->group(function() {
	Route::get('/get-countries', 'APIController@getCountries');

	Route::get('/get-places/{countryName}', 'APIController@getPlacesForCountry');

	Route::get('/get-place-details/{placeId}', 'APIController@getPlaceDetails');

	Route::get('/get-editors-pick', 'APIController@getEditorsPick');

	Route::post('/rate-place/{placeId}/{ratingValue}', 'APIController@ratePlace');
});

// Routes for the client admin dashboard
Route::prefix('client') -> group(function() {

	Route::get('/', 'ClientController@viewLoginPage');

	Route::get('/login', 'ClientController@viewLoginPage');

	Route::get('/signup', 'ClientController@viewSignupPage');

	Route::get('/dashboard', 'ClientController@viewDashboardPage')->middleware('guardClientPages');

	Route::get('/places', 'ClientController@viewPlacesPage')->middleware('guardClientPages');
	
	Route::get('/logout', 'ClientController@logoutClient');

	Route::post('/login', 'ClientController@loginClient');

	Route::post('/signup', 'ClientController@signupClient');

	// for the dashboard page
	Route::get('/get-dashboard-summary', 'ClientController@getDashboardSummary');
	
	Route::get('/get-client-dashboard-places-summary', 'ClientController@getDashboardPlacesSummary');

	// for the places page
	Route::post('/add-place', 'ClientController@addPlace');

	Route::get('/get-places', 'ClientController@getPlaces');

	Route::get('/place-details/{placeId}', 'ClientController@viewPlaceDetailsPage')->middleware('guardClientPages');

});


// Routes for the super adnin dashboard
Route::prefix('admin')->group(function() {
	Route::get('/', 'AdminController@viewLoginPage');

	Route::get('/login', 'AdminController@viewLoginPage');

	Route::post('/login', 'AdminController@loginAdmin');

	Route::post('/signup', 'AdminController@signupAdmin');

	Route::get('/signup', 'AdminController@viewSignupPage');

	Route::get('/dashboard', 'AdminController@viewDashboardPage')->middleware('guardAdminPages');

	Route::get('/countries', 'AdminController@viewCountriesPage')->middleware('guardAdminPages');

	Route::get('/places', 'AdminController@viewPlacesPage')->middleware('guardAdminPages');

	Route::get('/editors-pick', 'AdminController@viewEditorsPickPage')->middleware('guardAdminPages');

	Route::get('/users', 'AdminController@viewUsersPage')->middleware('guardAdminPages');

	Route::get('/logout', 'AdminController@logoutAdmin');

	// for the dashboard page
	Route::get('/get-dashboard-summary', 'AdminController@getDashboardSummary');

	Route::get('/get-dashboard-users-summary', 'AdminController@getDashboardUsersSummary');

	Route::get('/get-dashboard-places-summary', 'AdminController@getDashboardPlacesSummary');

	Route::get('/get-dashboard-editors-pick-summary', 'AdminController@getDashboardEditorsPickSummary');


	// for the countries page
	Route::post('/add-country', 'AdminController@addCountry');

	Route::post('/edit-country', 'AdminController@editCountry');

	Route::get('/get-countries', 'AdminController@getCountries');

	Route::post('/delete-country', 'AdminController@deleteCountry');

	// for the users page
	Route::get('/get-users', 'AdminController@getUsers');


	// for the places page
	Route::post('/add-place', 'AdminController@addPlace');

	Route::post('/edit-place', 'AdminController@editPlace');

	Route::get('/get-places', 'AdminController@getPlaces');

	Route::get('/place-details/{placeId}', 'AdminController@viewPlaceDetailsPage')->middleware('guardAdminPages');

	Route::post('/add-place-to-editor-pick', 'AdminController@togglePlaceEditorsPick');

	Route::post('/delete-place', 'AdminController@deletePlace');

	// for the editors picks
	Route::get('/get-editors-pick', 'AdminController@getEditorsPick');
});
