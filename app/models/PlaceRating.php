<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Place;

class PlaceRating extends Model
{
    protected $table = 'place_ratings';

    public $timestamps = false;

    public function places()
    {
        return $this->belongsTo('App\models\Place');
    }
}
