<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Place;

class PlaceImage extends Model
{
    protected $table = 'places_images';

    protected $primaryKey = 'place_image_id';

    public $timestamps = false;

    public function places()
    {
        return $this->belongsTo('App\models\Place');
    }
}
