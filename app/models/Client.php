<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $table = 'admins';

    protected $primaryKey = 'admin_id';

    public $timestamps = false;
}
