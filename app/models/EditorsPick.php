<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Place;

class EditorsPick extends Model
{
    protected $table = 'places_editors_pick';

    protected $primaryKey = 'editors_pick_id';

    public $timestamps = false;

    public function place()
    {
        return $this->belongsTo('App\models\Place');
    }
}
