<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\PlaceImage;
use App\models\EditorsPick;
use App\models\PlaceRating;

class Place extends Model
{
    protected $table = 'places';

    protected $primaryKey = 'place_id';

    public $timestamps = false;

    /**
     * Relationship between places and places images
     */
    public function placeImages()
    {
        return $this->hasMany('App\models\PlaceImage', 'place_image_id');
    }


    /**
     * Relationship between places and places ratings
     */
    public function placeRatings()
    {
        return $this->hasMany('App\models\PlaceRating', 'rating_place_id');
    }

    /**
     * Relationship between places and editors picks
     */
    public function editorsPick()
    {
        return $this->hasOne('App\models\EditorsPick', 'editors_pick_place_id');
    }
}
