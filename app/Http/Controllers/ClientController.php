<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\models\Admin;
use App\models\Place;
use App\models\PlaceImage;
use App\models\EditorsPick;
use JD\Cloudder\Facades\Cloudder;

class ClientController extends Controller
{
    /**
     *	Render the home page of the clients
     */
    public function viewLoginPage()
    {
    	return view('pages.admin_pages.index');
    }


    /**
     *	Render the home page of the clients
     */
    public function viewSignupPage()
    {
    	return view('pages.admin_pages.signup');
    }


    /**
     * Render the client dashboard page
     */
    public function viewDashboardPage()
    {
        return view('pages.admin_pages.dashboard');
    }


    /**
     * Render the client dashboard page
     */
    public function viewPlacesPage()
    {
        return view('pages.admin_pages.places');
    }


    /**
     * Render the place details page
     */
    public function viewPlaceDetailsPage($placeId)
    {
        $placeDetails = array();
        
        $place = Place::find($placeId);
        $placeId = $place['place_id'];
        $placeImages = Place::find($placeId)->placeImages;
        $place['place_images'] = $placeImages;

        if ($place != null) {
            return view('pages.admin_pages.place-details')->with('place', $place);
        } else {
            return abort(404);
        }

    }


    /**
     * Logout client
     */
    public function logoutClient()
    {
        session_start();
        unset($_SESSION['client_id']);
        unset($_SESSION['client_email']);
        return redirect('/client/');
    }


    /**
     * Perform client dashboard login
     *
     * @param Illuminate\Http\Request
     * @return JSON
     */
    public function loginClient(Request $request)
    {
    	$emailRequest = Admin::where('admin_email', $request->clientLoginEmail)->where('admin_type', 1)->get();

        if (count($emailRequest) > 0 ) {
            $storedPassword = $emailRequest[0]['admin_password'];
            $verifyPassword = md5($request->clientLoginPassword);
            if ($verifyPassword == $storedPassword) {
                session_start();
                $_SESSION['client_id'] = $emailRequest[0]['admin_id'];
                $_SESSION['client_email'] = $emailRequest[0]['admin_email'];

                $responseMessage =  array('success' => true, 'message' => 'Login successful');
                return response()->json($responseMessage);
            } else {
                $responseMessage =  array('success' => false, 'message' => 'Wrong email or password');
                return response()->json($responseMessage);
            }

        } else {
            $responseMessage =  array('success' => false, 'message' => 'Wrong email or password' );
            return response()->json($responseMessage);
        }
    	
    }


    public function signupClient(Request $request) 
    {
    	$emailRequest = Admin::where('admin_email', $request->clientSignupEmail)->get();

    	if (count($emailRequest) > 0 ) {
    		$responseMessage =  array('success' => false, 'message' => 'Email address already exists' );
    		return response()->json($responseMessage);
    		
    	} else {
    		$hashedPassword = md5($request->clientSignupPassword);
	    	$client = new Admin();

	    	$client->admin_username = $request->clientSignupUsername;
	    	$client->admin_password = $hashedPassword;
	    	$client->admin_email = $request->clientSignupEmail;
	    	$client->admin_type = 1;

	    	if ($client->save()) {
                session_start();
                $_SESSION['client_id'] = $client->admin_id;
                $_SESSION['client_email'] = $request->clientSignupEmail;
	    		$responseMessage =  array('success' => true, 'message' => 'signup successful' );
	    		return response()->json($responseMessage);
	    	} else {
	    		$responseMessage =  array('success' => false, 'message' => 'unable to signup' );
	    		return response()->json($responseMessage);
	    	}
    	}
    }

    /**
     * Add a place to the database
     *
     * @param Illuminate\Http\Request Request
     */
    public function addPlace(Request $request)
    {
        $getPlaceResponse = Place::where('place_name', $request->placeName)->where('place_country', $request->placeCountry)->get();

        if (count($getPlaceResponse) > 0) {
            $responseMessage =  array('success' => false, 'message' => 'Place with such details already exists' );
            return response()->json($responseMessage);
        } else {
            session_start();
            $place = new Place();
            $place->place_name = $request->placeName;
            $place->place_country = $request->placeCountry;
            $place->place_state = $request->placeState;
            $place->place_location = $request->placeLocation;
            $place->place_uploader_id = intval($_SESSION['client_id']);
            $place->place_description = $request->placeDescription;

            if ($place->save()) {
                $placeId = $place->place_id;
                $imageCount = 1;

                foreach ($request->file('placeImages') as $placeImage) {
                    $imagePublicId = "success-africa-place-$placeId-image-$imageCount";
                    $imageName = $placeImage->getRealPath();
                    Cloudder::upload($imageName, $imagePublicId, array('folder' => 'access_africa'));
                    $uploadResult = Cloudder::getResult();
                    $imageUrl = $uploadResult['secure_url'];

                    $placeImage = new PlaceImage();
                    $placeImage->place_image_id = $placeId;
                    $placeImage->place_image_url = $imageUrl;
                    $placeImage->save();

                    $imageCount += 1;
                }

                $responseMessage =  array('success' => true, 'message' => 'Place added successfully');
                return response()->json($responseMessage);
            } else {
                $responseMessage =  array('success' => false, 'message' => 'Could not add place. Please try again.');
                return response()->json($responseMessage);
            }

        }

    }

    /**
     * Get all places from the database
     */
    public function getPlaces()
    {   
        session_start();
        $places = Place::where('place_uploader_id', intval($_SESSION['client_id']))->orderBy('place_id', 'desc')->get();
        $placesResult = array();
        foreach ($places as $place) {
            $placeId = $place['place_id'];
            // get the place images for each place
            $placeImages = Place::find($placeId)->placeImages;
            $place['place_images'] = $placeImages;

            $allPlaceRatings = Place::find($placeId)->placeRatings;


            // get the rating for each place
            if (count($allPlaceRatings) > 0) {
                $placeRatingsSum = 0;
                foreach ($allPlaceRatings as $rating) {
                    $placeRatingsSum += $rating['rating_value'];
                }
                $place['place_rating'] = $placeRatingsSum / count($allPlaceRatings);
            } else {
                $place['place_rating'] = 0;
            }
            

            

            $editorsPick = Place::find($placeId)->editorsPick;
            if ($editorsPick != null) {
                $place['place_editors_pick'] = 1;
            } else {
                $place['place_editors_pick'] = 0;
            }

            array_push($placesResult, $place);
        }

        $responseMessage =  array('success' => true, 'message' => 'Places got', 'data' => $placesResult);
        return response()->json($responseMessage);
    }

    /**
     * Get admin dashboard summary
     */
    public function getDashboardSummary()
    {
        session_start();
        $numberOfPlace = count(Place::where('place_uploader_id', intval($_SESSION['client_id']))->orderBy('place_id', 'desc')->get()) | 0;

        $summaryData = array();
        $summaryData['numberOfPlace'] = $numberOfPlace;

        $responseMessage =  array('success' => true, 'message' => 'Summary dashboard got', 'data' => $summaryData );
        return response()->json($responseMessage);
    }


    /**
     * Get the admin dashboard places summary
     */
    public function getDashboardPlacesSummary()
    {
        session_start();
        $places = DB::table('places')->where('place_uploader_id', intval($_SESSION['client_id']))->limit(10)->get();
        $responseMessage =  array('success' => true, 'message' => 'Summary dashboard got', 'data' => $places );
        return response()->json($responseMessage);
    }
}
