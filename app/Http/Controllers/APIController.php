<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Admin;
use App\models\Country;
use App\models\Place;
use App\models\PlaceImage;
use App\models\EditorsPick;
use App\models\PlaceRating;

class APIController extends Controller
{
    /**
     * Get and return all countries
     */
    public function getCountries()
    {
        $countries =  Country::orderBy('country_name', 'asc')->get();
    	$responseMessage =  array('success' => true, 'message' => 'Countries fetched', 'data' => $countries );
    	return response()->json($responseMessage);
    }


    /**
     * Get places based on country specification
     */
    public function getPlacesForCountry($countryName)
    {
        $placeCountryQueryName = '%'.$countryName.'%';
        $places =  Place::where('place_country', 'like', $placeCountryQueryName)->get();
        $placesResult = array();
        foreach ($places as $place) {
            $placeId = $place['place_id'];
            $placeImages = Place::find($placeId)->placeImages;
            $place['place_images'] = $placeImages;

            $editorsPick = Place::find($placeId)->editorsPick;
            if ($editorsPick != null) {
                $place['place_editors_pick'] = 1;
            } else {
                $place['place_editors_pick'] = 0;
            }

            $placeRatings = Place::find($placeId)->placeRatings;
            if ($placeRatings != null) {
                if (count($placeRatings) < 1) {
                    $place['rating'] = 0.0;
                } else {
                    $ratingSum = 0;
                    foreach ($placeRatings as $rating) {
                        $ratingSum += $rating['rating_value'];
                    }

                    $totalRating = $ratingSum / count($placeRatings);
                    $place['rating'] = $totalRating;
                }

            } else {
                $place['rating'] = 0.0;
            }

            array_push($placesResult, $place);
        }
    	$responseMessage =  array('success' => true, 'message' => 'Countries fetched', 'data' => $places );
    	return response()->json($responseMessage);
    }


    /**
     * Get details of a particular place
     */
    function getPlaceDetails($placeId)
    {
        $place = Place::find($placeId);
        if ($place != null) {
            $placesResult = array();

            $placeImages = Place::find($placeId)->placeImages;
            $place['place_images'] = $placeImages;

            $editorsPick = Place::find($placeId)->editorsPick;
            if ($editorsPick != null) {
                $place['place_editors_pick'] = 1;
            } else {
                $place['place_editors_pick'] = 0;
            }

            $placeRatings = Place::find($placeId)->placeRatings;
            if ($placeRatings != null) {
                if (count($placeRatings) < 1) {
                    $place['rating'] = 0.0;
                } else {
                    $ratingSum = 0;
                    foreach ($placeRatings as $rating) {
                        $ratingSum += $rating['rating_value'];
                    }

                    $totalRating = $ratingSum / count($placeRatings);
                    $place['rating'] = $totalRating;
                }

            } else {
                $place['rating'] = 0.0;
            }

        } else {
            $place = array();
        }

    	$responseMessage =  array('success' => true, 'message' => 'Countries fetched', 'data' => $place );
    	return response()->json($responseMessage);
    }


    /**
     * Get all editor's pick
     */
    public function getEditorsPick()
    {
        $editorsPick = EditorsPick::all();
        $editorsPickResult = array();
        foreach ($editorsPick as $pick) {
            $editorsPickPlaceId = $pick['editors_pick_place_id'];
            $editorsPickPlace = Place::where('place_id', $editorsPickPlaceId)->get();
            array_push($editorsPickResult, $editorsPickPlace);
        }

        $responseMessage =  array('success' => true, 'message' => 'Editor pick got', 'data' => $editorsPickResult);
        return response()->json($responseMessage);
    }


    /**
     * Rate a place
     */
    public function ratePlace($placeId, $ratingValue)
    {
        $placeRating = new PlaceRating();
        $placeRating->rating_place_id = $placeId;
        $placeRating->rating_value = $ratingValue;

        if ($placeRating->save()) {
            $responseMessage =  array('success' => true, 'message' => 'Place rated successfully');
            return response()->json($responseMessage);
        } else {
            $responseMessage =  array('success' => false, 'message' => 'Could not rate place. Check your internet connection');
            return response()->json($responseMessage);
        }

    }
}
