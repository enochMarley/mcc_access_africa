<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\models\Admin;
use App\models\Country;
use App\models\Place;
use App\models\PlaceImage;
use App\models\EditorsPick;
use JD\Cloudder\Facades\Cloudder;

class AdminController extends Controller
{
	/**
     * Render the login page
     */
    public function viewLoginPage()
    {
    	return view('pages.super_admin_pages.index');
    }


    /**
     * Render the signup page
     */
    public function viewSignupPage()
    {
    	return view('pages.super_admin_pages.signup');
    }


    /**
     * Render the dashboard page
     */
    public function viewDashboardPage()
    {
    	return view('pages.super_admin_pages.dashboard');
    }



    /**
     * Render the countries page
     */
    public function viewCountriesPage()
    {
    	return view('pages.super_admin_pages.countries');
    }



    /**
     * Render the places page
     */
    public function viewPlacesPage()
    {
    	return view('pages.super_admin_pages.places');
    }

    /**
     * Render the place details page
     */
    public function viewPlaceDetailsPage($placeId)
    {
        $placeDetails = array();
        
        $place = Place::find($placeId);
        $placeId = $place['place_id'];
        $placeImages = Place::find($placeId)->placeImages;
        $place['place_images'] = $placeImages;

        if ($place != null) {
            return view('pages.super_admin_pages.place-details')->with('place', $place);
        } else {
            return abort(404);
        }

    }


    public function viewEditorsPickPage()
    {
    	return view('pages.super_admin_pages.editors-pick');
    }

    /**
     * Render the users page
     */
    public function viewUsersPage()
    {
    	return view('pages.super_admin_pages.users');
    }


    /**
     * Perform admin signup
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function signupAdmin(Request $request)
    {
    	$emailRequest = Admin::where('admin_email', $request->adminEmail)->get();

    	if (count($emailRequest) > 0 ) {
    		$responseMessage =  array('success' => false, 'message' => 'Email address already exists' );
    		return response()->json($responseMessage);

    	} else {
    		$hashedPassword = md5($request->adminPassword);
	    	$admin = new Admin();

	    	$admin->admin_username = $request->adminUsername;
	    	$admin->admin_password = $hashedPassword;
	    	$admin->admin_email = $request->adminEmail;
	    	$admin->admin_type = 0;

	    	if ($admin->save()) {
	    		$responseMessage =  array('success' => true, 'message' => 'signup successful' );
	    		return response()->json($responseMessage);
	    	} else {
	    		$responseMessage =  array('success' => false, 'message' => 'unable to signup' );
	    		return response()->json($responseMessage);
	    	}
    	}
    }


    /**
     * Perform admin login
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function loginAdmin(Request $request)
    {
    	$emailRequest = Admin::where('admin_email', $request->adminEmail)->where('admin_type', 0)->get();

    	if (count($emailRequest) > 0 ) {
    		$storedPassword = $emailRequest[0]['admin_password'];
    		$verifyPassword = md5($request->adminPassword);
    		if ($verifyPassword == $storedPassword) {
    			session_start();
    			$_SESSION['admin_id'] = $emailRequest[0]['admin_id'];
    			$_SESSION['admin_email'] = $emailRequest[0]['admin_email'];

    			$responseMessage =  array('success' => true, 'message' => 'Login successful');
    			return response()->json($responseMessage);
    		} else {
    			$responseMessage =  array('success' => false, 'message' => 'Wrong email or password');
    			return response()->json($responseMessage);
    		}

    	} else {
    		$responseMessage =  array('success' => false, 'message' => 'Wrong email or password' );
    		return response()->json($responseMessage);
    	}
    }


    /**
     * Logout admin from the system
     */
    public function logoutAdmin()
    {
    	session_start();
    	unset($_SESSION['admin_id']);
        unset($_SESSION['admin_email']);
    	return redirect('/admin/');
    }

    /**
     * Get admin dashboard summary
     */
    public function getDashboardSummary()
    {
        $numberOfUsers = count(Admin::all()) | 0;
        $numberOfPlace = count(Place::all()) | 0;
        $numberOfCountries = count(Country::all())| 0;
        $numberOfEditorsPick = count(EditorsPick::all()) | 0;

        $summaryData = array();
        $summaryData['numberOfUsers'] = $numberOfUsers;
        $summaryData['numberOfPlace'] = $numberOfPlace;
        $summaryData['numberOfCountries'] = $numberOfCountries;
        $summaryData['numberOfEditorsPick'] = $numberOfEditorsPick;

        $responseMessage =  array('success' => true, 'message' => 'Summary dashboard got', 'data' => $summaryData );
        return response()->json($responseMessage);
    }


    /**
     * Get the admin dashboard users summary
     */
    public function getDashboardUsersSummary()
    {
        $users = DB::table('admins')->limit(10)->get();
        $responseMessage =  array('success' => true, 'message' => 'Summary dashboard got', 'data' => $users );
        return response()->json($responseMessage);
    }


    /**
     * Get the admin dashboard places summary
     */
    public function getDashboardPlacesSummary()
    {
        $places = DB::table('places')->limit(10)->get();
        $responseMessage =  array('success' => true, 'message' => 'Summary dashboard got', 'data' => $places );
        return response()->json($responseMessage);
    }


    /**
     * Get the admin dashboard places summary
     */
    public function getDashboardEditorsPickSummary()
    {
        $editorsPick = DB::table('places_editors_pick')->limit(10)->get();
        $editorsPickResult = array();
        $editorsPick  = json_decode($editorsPick, true);
        
        foreach ($editorsPick as $pick) {
            $editorsPickPlaceId = $pick['editors_pick_place_id'];
            $editorsPickPlace = Place::where('place_id', $editorsPickPlaceId)->get();
            array_push($editorsPickResult, $editorsPickPlace);
        }

        $responseMessage =  array('success' => true, 'message' => 'Places got', 'data' => $editorsPickResult);
        return response()->json($responseMessage);
    }


    /**
     * Add country to database
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function addCountry(Request $request)
    {
    	$countryRequest = Country::where('country_name', $request->countryName)->get();

    	if (count($countryRequest) > 0 ) {
    		$responseMessage =  array('success' => false, 'message' => 'Country already exists' );
    		return response()->json($responseMessage);

    	} else {
    		$country = new Country();

    		$country->country_name = $request->countryName;

    		if ($country->save()) {
    			$responseMessage =  array('success' => true, 'message' => 'Country added successfully' );
    			return response()->json($responseMessage);
    		} else {
    			$responseMessage =  array('success' => false, 'message' => 'Could not add country. Please try again' );
    			return response()->json($responseMessage);
    		}

    	}
    }


    /**
     * Update country name in the database
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function editCountry(Request $request)
    {
    	$countryRequest = Country::where('country_name', $request->countryName)->get();

    	if (count($countryRequest) > 0 ) {
    		$responseMessage =  array('success' => false, 'message' => 'Country already exists' );
    		return response()->json($responseMessage);

    	} else {
    		$country = Country::find($request->countryId);

    		$country->country_name = $request->countryName;

    		if ($country->save()) {
    			$responseMessage =  array('success' => true, 'message' => 'Country updated successfully' );
    			return response()->json($responseMessage);
    		} else {
    			$responseMessage =  array('success' => false, 'message' => 'Could not update country details. Please try again' );
    			return response()->json($responseMessage);
    		}

    	}
    }


    /**
     * Get all countries from the database
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function getCountries(Request $request)
    {
    	$countries =  Country::orderBy('country_name', 'asc')->get();
    	$responseMessage =  array('success' => true, 'message' => 'Countries fetched', 'data' => $countries );
    	return response()->json($responseMessage);
    }


    /**
     * Delete a country from the database
	 *
	 * @param Illuminate\Http\Request Request
     */
    function deleteCountry(Request $request)
    {
    	$country = Country::find($request->countryId);
    	$country->delete();
    	$responseMessage =  array('success' => true, 'message' => 'Country deleted successfully' );
    	return response()->json($responseMessage);
    }


    /**
     * Delete a country from the database
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function getUsers(Request $request)
    {
    	$users =  Admin::all();
    	$responseMessage =  array('success' => true, 'message' => 'Could not add country. Please try again', 'data' => $users );
    	return response()->json($responseMessage);
    }


    /**
     * Add a place to the database
	 *
	 * @param Illuminate\Http\Request Request
     */
    public function addPlace(Request $request)
    {
    	$getPlaceResponse = Place::where('place_name', $request->placeName)->where('place_country', $request->placeCountry)->get();

    	if (count($getPlaceResponse) > 0) {
    		$responseMessage =  array('success' => false, 'message' => 'Place with such details already exists' );
    		return response()->json($responseMessage);
    	} else {
            session_start();
    		$place = new Place();
    		$place->place_name = $request->placeName;
    		$place->place_country = $request->placeCountry;
    		$place->place_state = $request->placeState;
            $place->place_location = $request->placeLocation;
    		$place->place_uploader_id = intval($_SESSION['admin_id']);
    		$place->place_description = $request->placeDescription;

    		if ($place->save()) {
    			$placeId = $place->place_id;
    			$imageCount = 1;

    			foreach ($request->file('placeImages') as $placeImage) {
    				$imagePublicId = "success-africa-place-$placeId-image-$imageCount";
    				$imageName = $placeImage->getRealPath();
    				Cloudder::upload($imageName, $imagePublicId, array('folder' => 'access_africa'));
    				$uploadResult = Cloudder::getResult();
    				$imageUrl = $uploadResult['secure_url'];

    				$placeImage = new PlaceImage();
    				$placeImage->place_image_id = $placeId;
    				$placeImage->place_image_url = $imageUrl;
    				$placeImage->save();

    				$imageCount += 1;
    			}

    			$responseMessage =  array('success' => true, 'message' => 'Place added successfully');
    			return response()->json($responseMessage);
    		} else {
    			$responseMessage =  array('success' => false, 'message' => 'Could not add place. Please try again.');
    			return response()->json($responseMessage);
    		}

    	}

    }


    /**
     * Edit a place in the database
     *
     * @param Illuminate\Http\Request Request
     */
    public function editPlace(Request $request)
    {
        $place = Place::find($request->placeId);

        $place->place_name = $request->placeName;
        $place->place_country = $request->placeCountry;
        $place->place_state = $request->placeState;
        $place->place_location = $request->placeLocation;
        $place->place_description = $request->placeDescription;

        if ($place->save()) {
            $responseMessage =  array('success' => true, 'message' => 'Place details updated successfully' );
            return response()->json($responseMessage);
        } else {
            $responseMessage =  array('success' => false, 'message' => 'Could not update place details. Please try again' );
            return response()->json($responseMessage);
        }
  
    }

    /**
     * Get all places from the database
     */
    public function getPlaces()
    {
        $places = Place::orderBy('place_id', 'desc')->get();
        $placesResult = array();
        foreach ($places as $place) {
            $placeId = $place['place_id'];
            // get the place images for each place
            $placeImages = Place::find($placeId)->placeImages;
            $place['place_images'] = $placeImages;

            $allPlaceRatings = Place::find($placeId)->placeRatings;


            // get the rating for each place
            if (count($allPlaceRatings) > 0) {
                $placeRatingsSum = 0;
                foreach ($allPlaceRatings as $rating) {
                    $placeRatingsSum += $rating['rating_value'];
                }
                $place['place_rating'] = $placeRatingsSum / count($allPlaceRatings);
            } else {
                $place['place_rating'] = 0;
            }
            

            

            $editorsPick = Place::find($placeId)->editorsPick;
            if ($editorsPick != null) {
                $place['place_editors_pick'] = 1;
            } else {
                $place['place_editors_pick'] = 0;
            }

            array_push($placesResult, $place);
        }

        $responseMessage =  array('success' => true, 'message' => 'Places got', 'data' => $placesResult);
        return response()->json($responseMessage);
    }


    /**
     * Add place to editors pick
     * @param Illuminate\Http\Request Request
     */
    public function togglePlaceEditorsPick(Request $request)
    {
        $editorsPickRequest = EditorsPick::where('editors_pick_place_id', $request->placeId)->get();
        if (count($editorsPickRequest) > 0) {
            $editorsPick = EditorsPick::find($editorsPickRequest[0]['editors_pick_id']);
            if ($editorsPick->delete()) {
                $responseMessage =  array('success' => true, 'message' => 'Place removed editors pick');
                return response()->json($responseMessage);
            }
        } else {
            $editorsPick = new EditorsPick();
            $editorsPick->editors_pick_place_id = $request->placeId;
            if ($editorsPick->save()) {
                $responseMessage =  array('success' => true, 'message' => 'Place added to editors pick');
                return response()->json($responseMessage);
            } else {
                $responseMessage =  array('success' => false, 'message' => 'Could not add place to editors pick');
                return response()->json($responseMessage);
            }

        }

    }


    /**
     * Delete a place from the database
     * @param Illuminate\Http\Request Request
     */
    public function deletePlace(Request $request)
    {
        $place = Place::find($request->placeId);

        if ($place->delete()) {
            $responseMessage =  array('success' => true, 'message' => 'Places deleted successfully');
            return response()->json($responseMessage);
        } else {
            $responseMessage =  array('success' => false, 'message' => 'Could not delete place. Please try again');
            return response()->json($responseMessage);
        }

    }


    /**
     * Get all editor's pick
     */
    public function getEditorsPick()
    {
        $editorsPick = EditorsPick::all();
        $editorsPickResult = array();
        foreach ($editorsPick as $pick) {
            $editorsPickPlaceId = $pick['editors_pick_place_id'];
            $editorsPickPlace = Place::where('place_id', $editorsPickPlaceId)->get();
            array_push($editorsPickResult, $editorsPickPlace);
        }

        $responseMessage =  array('success' => true, 'message' => 'Places got', 'data' => $editorsPickResult);
        return response()->json($responseMessage);
    }
}
