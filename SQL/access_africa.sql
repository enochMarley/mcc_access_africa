-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2018 at 05:39 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `access_africa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_username`, `admin_email`, `admin_password`, `admin_type`) VALUES
(1, 'usernname', 'email@address.com', 'userpassword', 0),
(7, 'username', 'mail@mail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`) VALUES
(2, 'Ghana'),
(3, 'Nigeria'),
(4, 'South Africa');

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `place_id` int(11) NOT NULL,
  `place_name` varchar(255) NOT NULL,
  `place_country` varchar(255) NOT NULL,
  `place_state` varchar(255) NOT NULL,
  `place_location` varchar(255) NOT NULL,
  `place_description` text NOT NULL,
  `place_uploader_id` int(11) NOT NULL,
  `place_upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`place_id`, `place_name`, `place_country`, `place_state`, `place_location`, `place_description`, `place_uploader_id`, `place_upload_date`) VALUES
(7, 'Name of venue', 'South Africa', 'j\'burgh', 'soweti', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 0, '2018-09-17 04:42:03'),
(8, 'Name of venue', 'Nigeria', 'j\'burgh', 'soweti', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 0, '2018-09-17 04:42:49'),
(9, 'Name of venue', 'Ghana', 'j\'burgh', 'soweti', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 0, '2018-09-17 04:46:43'),
(10, 'Name of venue new', 'Ghana', 'j\'burgh', 'soweti', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 0, '2018-09-17 04:49:26'),
(11, 'fkjdslkfjd dsfjdsklfj', 'Ghana', 'skdjfkldsj', 'jdflkdjsfkd', 'jsdlkfjd sfjsdfkjdsf dskfjdslkfjkdsfjk', 0, '2018-09-17 04:51:12'),
(12, 'fkjdslkfjd  kdfldsjfkd', 'Ghana', 'skdjfkldsj', 'jdflkdjsfkd', 'jsdlkfjd sfjsdfkjdsf dskfjdslkfjkdsfjk', 0, '2018-09-17 04:52:21');

-- --------------------------------------------------------

--
-- Table structure for table `places_editors_pick`
--

CREATE TABLE `places_editors_pick` (
  `editors_pick_id` int(11) NOT NULL,
  `editors_pick_place_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places_editors_pick`
--

INSERT INTO `places_editors_pick` (`editors_pick_id`, `editors_pick_place_id`) VALUES
(7, 12);

-- --------------------------------------------------------

--
-- Table structure for table `places_images`
--

CREATE TABLE `places_images` (
  `place_image_id` int(11) NOT NULL,
  `place_image_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places_images`
--

INSERT INTO `places_images` (`place_image_id`, `place_image_url`) VALUES
(8, 'access_africa/success-africa-place-8-image-1'),
(8, 'access_africa/success-africa-place-8-image-1'),
(8, 'access_africa/success-africa-place-8-image-1'),
(11, 'https://res.cloudinary.com/dbxbf69uu/image/upload/v1537159893/access_africa/success-africa-place-11-image-1.jpg'),
(12, 'https://res.cloudinary.com/dbxbf69uu/image/upload/v1537159965/access_africa/success-africa-place-12-image-1.jpg'),
(12, 'https://res.cloudinary.com/dbxbf69uu/image/upload/v1537159970/access_africa/success-africa-place-12-image-2.jpg'),
(12, 'https://res.cloudinary.com/dbxbf69uu/image/upload/v1537159974/access_africa/success-africa-place-12-image-3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `place_ratings`
--

CREATE TABLE `place_ratings` (
  `rating_place_id` int(11) NOT NULL,
  `rating_value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `place_ratings`
--

INSERT INTO `place_ratings` (`rating_place_id`, `rating_value`) VALUES
(11, 4.5),
(11, 3.5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`place_id`);

--
-- Indexes for table `places_editors_pick`
--
ALTER TABLE `places_editors_pick`
  ADD PRIMARY KEY (`editors_pick_id`),
  ADD KEY `edit_pick_err` (`editors_pick_place_id`);

--
-- Indexes for table `places_images`
--
ALTER TABLE `places_images`
  ADD KEY `place_img_err` (`place_image_id`);

--
-- Indexes for table `place_ratings`
--
ALTER TABLE `place_ratings`
  ADD KEY `place_rating_err` (`rating_place_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `place_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `places_editors_pick`
--
ALTER TABLE `places_editors_pick`
  MODIFY `editors_pick_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `places_editors_pick`
--
ALTER TABLE `places_editors_pick`
  ADD CONSTRAINT `edit_pick_err` FOREIGN KEY (`editors_pick_place_id`) REFERENCES `places` (`place_id`);

--
-- Constraints for table `places_images`
--
ALTER TABLE `places_images`
  ADD CONSTRAINT `place_img_err` FOREIGN KEY (`place_image_id`) REFERENCES `places` (`place_id`);

--
-- Constraints for table `place_ratings`
--
ALTER TABLE `place_ratings`
  ADD CONSTRAINT `place_rating_err` FOREIGN KEY (`rating_place_id`) REFERENCES `places` (`place_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
