@extends('layouts.app')

@section('content')
	<nav class="navbar fixed-top navbar-expand-lg navbar-light admin-custom-navbar">
	  	<a class="navbar-brand" href="#">Navbar</a>
	  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	  	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          <span class="lnr lnr-user"></span> {{ $_SESSION['admin_email']}}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="/admin/logout"><span class="lnr lnr-power-switch"></span> Logout</a>
		        </div>
		      </li>
		    </ul>
		</div>
	</nav>

	<div class="container-fluid admin-dashboard-main-div">
		<div class="row">
			<div class="col-md-2 admin-dashboard-side-div">
				<p class="admin-dashboard-side-div-title"><small>ADMIN DASHBOARD</small></p>
				<li><a href="/admin/dashboard"><span class="lnr lnr-pie-chart"></span> Dashboard</a></li>
				<li class="selected"><a href="/admin/countries"><span class="lnr lnr-flag"></span> Countries</a></li>
				<li><a href="/admin/places"><span class="lnr lnr-map-marker"></span> Places</a></li>
				<li><a href="/admin/editors-pick"><span class="lnr lnr-star"></span> Editor's Pick</a></li>
				<li><a href="/admin/users"><span class="lnr lnr-users"></span> Users</a></li>
				<hr>
				<li><a href="/admin/logout"><span class="lnr lnr-power-switch"></span> Logout</a></li>
			</div>
			<div class="col-md-10 admin-dashboard-content-div">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-6 countries-res">
						
					</div>
					<div class="col-md-4"></div>
				</div>
				<button class="btn floating-btn" data-toggle="modal" data-target="#admin-add-country-modal">+</button>
			</div>
		</div>
	</div>

	<div class="modal fade" id="admin-add-country-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add Country</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="add-country-form" method="post">
                    <div class="form-group">
                        <label><small>Country Name</small></label>
                        <input type="text" class="form-control form-control-sm add-country-name" required>
                    </div>
                    <button type="submit"class="btn btn-primary btn-sm">Add</button>
                </form>
              </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="admin-edit-country-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Country</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="edit-country-form" method="post">
                	<input type="hidden" name="" class="edit-country-id">
                    <div class="form-group">
                        <label><small>Country Name</small></label>
                        <input type="text" class="form-control form-control-sm edit-country-name" required>
                    </div>
                    <button type="submit"class="btn btn-primary btn-sm">Update</button>
                </form>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/controllers/admin-countries-controller.js') }}"></script>
	<script type="text/javascript">
		getAllCountries();
	</script>
@endsection