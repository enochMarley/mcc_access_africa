@extends('layouts.app')

@section('stylesheet')

@endsection

@section('content')
    <div class="container-fluid login-signup-div">
    	<div class="row">
    		<div class="col-md-4"></div>
    		<div class="col-md-4">
                <div class="login-signup-form-div">

                    <div class="card login-signup-form-input-div admin-login-signup-form-input-div">
                    	<h4 class="text-center">ACCESS AFRICA</h4>
                        <h4 class="text-center">Admin Login</h4><br>

                        <form class="admin-login-form">
                            
                            <div class="form-group">
                                <input type="email" name="" class="form-control form-control-sm admin-login-email text-center" placeholder="Email Address" required>
                            </div>

                            <div class="form-group">
                                <input type="password" name="" class="form-control form-control-sm admin-login-password text-center" placeholder="Password" required>
                            </div>

                            <div class="btn-div">
                                <button type="submit" class="btn btn-sm btn-outline-info">GET STARTED</button><br><br>
                                <a href="/admin/signup"><p class="text-center"><small>create an admin account</small></p></a>
                            </div>

                            
                        </form>

                        
                    </div>
                </div>
    		</div>
    		<div class="col-md-4"></div>
    	</div>
    </div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/controllers/admin-auth-controller.js') }}"></script>
@endsection