@extends('layouts.app')

@section('content')
	<nav class="navbar fixed-top navbar-expand-lg navbar-light admin-custom-navbar">
	  	<a class="navbar-brand" href="#">Navbar</a>
	  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	  	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          <span class="lnr lnr-user"></span> {{ $_SESSION['admin_email']}}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="/admin/logout"><span class="lnr lnr-power-switch"></span> Logout</a>
		        </div>
		      </li>
		    </ul>
		</div>
	</nav>

	<div class="container-fluid admin-dashboard-main-div">
		<div class="row">
			<div class="col-md-2 admin-dashboard-side-div">
				<p class="admin-dashboard-side-div-title"><small>ADMIN DASHBOARD</small></p>
				<li><a href="/admin/dashboard"><span class="lnr lnr-pie-chart"></span> Dashboard</a></li>
				<li><a href="/admin/countries"><span class="lnr lnr-flag"></span> Countries</a></li>
				<li><a href="/admin/places"><span class="lnr lnr-map-marker"></span> Places</a></li>
				<li class="selected"><a href="/admin/editors-pick"><span class="lnr lnr-star"></span> Editor's Pick</a></li>
				<li><a href="/admin/users"><span class="lnr lnr-users"></span> Users</a></li>
				<hr>
				<li><a href="/admin/logout"><span class="lnr lnr-power-switch"></span> Logout</a></li>
			</div>
			<div class="col-md-10 admin-dashboard-content-div">
				<div class="row">
					<div class="col-md-12 editors-pick-res"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/controllers/admin-editors-pick-controller.js') }}"></script>
	<script type="text/javascript">
	</script>
@endsection
