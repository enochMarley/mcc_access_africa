@extends('layouts.app')

@section('content')
	<nav class="navbar fixed-top navbar-expand-lg navbar-light admin-custom-navbar">
	  	<a class="navbar-brand" href="#">Navbar</a>
	  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	  	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          <span class="lnr lnr-user"></span> {{ $_SESSION['client_email']}}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="/client/logout"><span class="lnr lnr-power-switch"></span> Logout</a>
		        </div>
		      </li>
		    </ul>
		</div>
	</nav>

	<div class="container-fluid admin-dashboard-main-div">
		<div class="row">
			<div class="col-md-2 admin-dashboard-side-div">
				<p class="admin-dashboard-side-div-title"><small>ADMIN DASHBOARD</small></p>
				<li><a href="/client/dashboard"><span class="lnr lnr-pie-chart"></span> Dashboard</a></li>
				<li class="selected"><a href="/client/places"><span class="lnr lnr-map-marker"></span>My Places</a></li>
				<hr>
				<li><a href="/client/logout"><span class="lnr lnr-power-switch"></span> Logout</a></li>
			</div>

			<div class="col-md-10 admin-dashboard-content-div">
				<h5>Place Details: {{ $place->place_name }} </h5>
                <div class="row">
                    <div class="col-md-6">
                        @if (count($place->place_images) < 1)
                            <div class="jumbotron">
                                <h5 class="text-center"><small>No Images Provided</small></h5>
                            </div>
                        @else

                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach ($place->place_images as $image_key => $place_image)
                                        @if($image_key == 0)
                                            <div class="carousel-item place-details-slide-img-div active">
                                                <img class="d-block w-100" src="https://res.cloudinary.com/dbxbf69uu/image/upload/v1539102809/Qwiqspace/venues/Qwiqspace-venue-a5bb9ce392-image-3.jpg" class="img-fluid" alt="First slide">
                                            </div>
                                        @else
                                            <div class="carousel-item place-details-slide-img-div">
                                                <img class="d-block w-100" src="https://res.cloudinary.com/dbxbf69uu/image/upload/v1539102809/Qwiqspace/venues/Qwiqspace-venue-a5bb9ce392-image-3.jpg" class="img-fluid" alt="First slide">
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                            </div>
                        @endif
                    </div>

                    <div class="col">
                        <table class="table">
                            <tbody>
                                <tr>
                                  <th scope="row">Name</th>
                                  <td>{{ $place->place_name }}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Country</th>
                                  <td>{{ $place->place_country }}</td>
                                </tr>
                                <tr>
                                  <th scope="row">State</th>
                                  <td>{{ $place->place_state }}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Location</th>
                                  <td>{{ $place->place_location }}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Description</th>
                                  <td>{{ $place->place_description }}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Upload Date</th>
                                  <td>{{ $place->place_upload_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                </div>
			</div>
		</div>
	</div>

	
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/controllers/client-places-controller.js') }}"></script>
	<script type="text/javascript">
		getAllCountriesForForm();
	</script>
@endsection