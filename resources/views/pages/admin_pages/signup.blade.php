@extends('layouts.app')

@section('stylesheet')
    <style type="text/css">
        body {
            background-image: url({{ asset('images/access-africa-auth-bg.jpg') }});
            background-repeat: no-repeat;
            background-size: cover;
            background-origin: content-box;
            background-attachment: fixed;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid login-signup-div">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="login-signup-form-div">
                    <div class="card login-signup-form-input-div">
                        <img src="{{ asset('images/access-africa-logo.jpg') }}" class="client-login-logo" alt="">
                        <br>
                        <form class="client-signup-form">

                             <div class="form-group">
                                <input type="text" name="" class="form-control form-control-sm client-signup-username text-center" placeholder="Username" required>
                            </div>
                            
                            <div class="form-group">
                                <input type="email" name="" class="form-control form-control-sm client-signup-email text-center" placeholder="Email Address" required>
                            </div>

                            <div class="form-group">
                                <input type="password" name="" class="form-control form-control-sm client-signup-password text-center" placeholder="Password" required>
                            </div>

                            <div class="form-group">
                                <input type="password" name="" class="form-control form-control-sm client-signup-confirm-password text-center" placeholder="Confirm Password" required>
                            </div>

                            <div class="btn-div">
                                <button type="submit" class="btn btn-sm btn-outline-info">CREATE ACCOUNT</button><br><br>
                                <a href="/client/login"><p class="text-center"><small>login to account</small></p></a>
                            </div>

                            
                        </form>

                        
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/controllers/client-auth-controller.js') }}"></script>
@endsection