@extends('layouts.app')

@section('content')
	<nav class="navbar fixed-top navbar-expand-lg navbar-light admin-custom-navbar">
	  	<a class="navbar-brand" href="#">Navbar</a>
	  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	  	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          <span class="lnr lnr-user"></span> {{ $_SESSION['client_email']}}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="/client/logout"><span class="lnr lnr-power-switch"></span> Logout</a>
		        </div>
		      </li>
		    </ul>
		</div>
	</nav>

	<div class="container-fluid admin-dashboard-main-div">
		<div class="row">
			<div class="col-md-2 admin-dashboard-side-div">
				<p class="admin-dashboard-side-div-title"><small>ADMIN DASHBOARD</small></p>
				<li class="selected"><a href="/client/dashboard"><span class="lnr lnr-pie-chart"></span> Dashboard</a></li>
				<li><a href="/client/places"><span class="lnr lnr-map-marker"></span>My Places</a></li>
				<hr>
				<li><a href="/client/logout"><span class="lnr lnr-power-switch"></span> Logout</a></li>
			</div>
			<div class="col-md-10 admin-dashboard-content-div">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-summary-div alert alert-success">
							<h6><span class="lnr lnr-map-marker"></span> My Places</h6>
							<h3 class="number-of-places-res">0</h3>
						</div>
					</div>
				</div>
				<br>
				
				<div class="row">
					<div class="col-md-6">
						<div class="admin-places-summary-div"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/controllers/client-dashboard-controller.js') }}"></script>
@endsection